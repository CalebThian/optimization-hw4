function [c ceq]=gg(x)
    c=[];
    ceq=2*x(1)+x(2)+pi*x(2)/2-10;
end