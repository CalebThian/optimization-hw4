function val = m_of_grad(x)
    [d1 d2 d3]=grad(x);
    val=(d1^2+d2^2+d3^2)^0.5;
end