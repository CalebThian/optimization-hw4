function val=f(x)
    %f(a,b)=a*b+pi*b^2/8
    val=-x(1)*x(2)-(pi*x(2)^2)/8;
end