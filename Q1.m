% 1.	A window is being built with the shape as the combination of a 
% rectangle and a semicircle. If there is 10 meters of framing materials, 
% what must the dimensions of the window be to let in the most light?
% (That is, maximize the area of the window with the rim of the 
% window being equal to 10 meters.)
% Please solve this problem numerically instead of by hand calculation.

% Solution
% Assume the length and width of the shape is a and b,
% the rim R(a,b) = 2a+b+pi*b/2=10,a>0,b>0;
% The area is equal to f(a,b)=a*b+pi*b^2/8=b(a+pi*b/8)
% A=[0.5*coef. of a^2,coef. of ab;coef. of ab, 0.5*coef of b^2]
% B=[coef. of a, coef. of b]
global X S
A=[0 1;1 pi/8];
b=[0;0];
x=[1;1];
k=1;
r=1;
c=2;
X=[x;r];
%g(x)=2a+b+pi*b/2-10
%Minimize phi(x,r)=f(x)+r*sum(G(g(x))) where G(g(x))={max(0,g(x))}^2,
% Using BFGS method
eps=0.01;
while(1)
    B=eye(3);
    df=[0;0;0];
    [df(1) df(2) df(3)]=grad(X);
    df2=df;
    while(1)
        mg=m_of_grad(X);
        if m_of_grad(X)<eps
            break
        else
            df=df2;
            S=-B*df;
            lamda=goldSearch(@fline,-10,10);
            d=lamda*S;
            X=X+lamda*S;
            mg=m_of_grad(X);
            if m_of_grad(X)<eps
                break
            else
                if lamda~=10 || lamda~=-10
                    [df2(1) df2(2) df2(3)]=grad(X);
                    g=df2-df;
                    M=(1+g'*B*g/(d'*g))*d*d'/(d'*g);
                    N=-d*g'*B/(d'*g);
                    Q=-B*g*d'/(d'*g);
                    B=B+M+N+Q;
                end
            end
        end
    end

    x=[X(1) X(2)];
    xcomp=[];
    for i=0:1
        for j=0:1
            xcomp=[xcomp;X(1)+i*10*eps X(2)+i*10*eps];
        end
    end
    dim=[1 1 1 1]
    xcomp=mat2cell(xcomp,dim);

    xt=cell2mat(xcomp(1));
    X(1)=xt(1);X(2)=xt(2);
    fv=f(X);
    ok=1;
    for i=2:4
        xt=cell2mat(xcomp(i));
        X(1)=xt(1);X(2)=xt(2);
        if fv<f(X)
            ok=0;
            break
        end
    end
    
    if ok==1
        break;
    end
end
        
