function val = z(F)
    global A0
    n=2;
    val=0;
    for i=1:9
        val=val+(F(i)/A0(i))^n;
    end
end

