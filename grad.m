function [d1 d2 d3]=grad(x)
% phi(x,r)=f(x)+r*sum(G(g(x)))
    % f(a,b)=a*b+pi*b^2/8
    % g(x)=2a+b+pi*b/2-10
    gi=g(x);

	%phi(x,r)= a*b+pi*b^2/8+r(2a+b+pi*b/2-10)^2
	%d/da=b+4r(g) 
	d1=x(2)+4*x(3)*gi;
	%d/db=a+pi*b/4+2r(g)(1+pi/2)
    d2=x(1)+pi*x(2)/4+2*x(3)*gi*(1+pi/2);
	%d/dr=g^2
    d3=gi^2;
end