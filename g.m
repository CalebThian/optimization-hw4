function val=g(x)
%g(x)=2a+b+pi*b/2-10
val=2*x(1)+x(2)+pi*x(2)/2-10;
end